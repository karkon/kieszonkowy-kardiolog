package com.michalt.kieszonkowykardiolog.Model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class ExtendedMeasurement extends SimpleMeasurement implements Parcelable {
    private double cp; //chestPainType
    private double chol; //cholesterol
    private double fbs; //bloodSugar
 //  private int restecg;
    private double exang; //painType
   // private int oldpeak;
  //  private int slope;
   // private int ca;
  //  private int thal;
    private int prediction = -1;

    public ExtendedMeasurement(double age, String sex, double thalach, double highBloodPreasure, double trestbps, double weight, double height, double cp, double chol, double fbs, double exang) {
        super(age, sex, thalach, highBloodPreasure, trestbps, weight, height);
        this.cp = cp;
        this.chol = chol;
        this.fbs = fbs;
        this.exang = exang;
    }

    // porzebny przy wczytywaniu z bazy
    public ExtendedMeasurement(String measurementName, double age, String sex, double thalach, double highBloodPressure, double trestbps, Date dateTime, double weight, double height, double cp, double chol, double fbs, double exang, int prediction) {
        super(measurementName, age, sex, thalach, highBloodPressure, trestbps, dateTime, weight, height);
        this.cp = cp;
        this.chol = chol;
        this.fbs = fbs;
        this.exang = exang;
        this.prediction = prediction;
    }

    public ExtendedMeasurement() {
    }

    protected ExtendedMeasurement(Parcel in) {
        super(in);
        cp = in.readDouble();
        chol = in.readDouble();
        fbs = in.readDouble();
        exang = in.readDouble();
        prediction = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeDouble(cp);
        dest.writeDouble(chol);
        dest.writeDouble(fbs);
        dest.writeDouble(exang);
        dest.writeInt(prediction);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ExtendedMeasurement> CREATOR = new Creator<ExtendedMeasurement>() {
        @Override
        public ExtendedMeasurement createFromParcel(Parcel in) {
            return new ExtendedMeasurement(in);
        }

        @Override
        public ExtendedMeasurement[] newArray(int size) {
            return new ExtendedMeasurement[size];
        }
    };

    public double getCp() {
        return cp;
    }
    public void setCp(double cp) {
        this.cp = cp;
    }
    public double getChol() {
        return chol;
    }
    public void setChol(double chol) {
        this.chol = chol;
    }
    public double getFbs() {
        return fbs;
    }
    public void setFbs(double fbs) {
        this.fbs = fbs;
    }
    public double getExang() {
        return exang;
    }
    public void setExang(double exang) {
        this.exang = exang;
    }

    public int getPrediction() {
        return prediction;
    }
    public void setPrediction(int prediction) {
        this.prediction = prediction;
    }
}
