package com.michalt.kieszonkowykardiolog.Model;

import android.icu.text.SimpleDateFormat;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class PressurePlotData implements Parcelable {
    private double highBloodPressure; //highBloodPressure
    private double trestbps; //lowbloodPreasure
    private Date date;
    private String dateString;


    public PressurePlotData(double highBloodPressure, double trestbps, Date date) {
        this.highBloodPressure = highBloodPressure;
        this.trestbps = trestbps;
        this.date = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy_HH:mm");
        this.dateString  = dateFormat.format(date);
    }

    public PressurePlotData() {

    }

    protected PressurePlotData(Parcel in) {
        highBloodPressure = in.readDouble();
        trestbps = in.readDouble();
        dateString = in.readString();
        date = (Date) in.readSerializable();
    }

    public static final Creator<PressurePlotData> CREATOR = new Creator<PressurePlotData>() {
        @Override
        public PressurePlotData createFromParcel(Parcel in) {
            return new PressurePlotData(in);
        }

        @Override
        public PressurePlotData[] newArray(int size) {
            return new PressurePlotData[size];
        }
    };

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public double getHighBloodPressure() {
        return highBloodPressure;
    }

    public void setHighBloodPressure(double highBloodPleasure) {
        this.highBloodPressure = highBloodPleasure;
    }

    public double getTrestbps() {
        return trestbps;
    }

    public void setTrestbps(double trestbps) {
        this.trestbps = trestbps;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(highBloodPressure);
        dest.writeDouble(trestbps);
        dest.writeString(dateString);
        dest.writeSerializable(date);
    }
}
