package com.michalt.kieszonkowykardiolog.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class User  implements Parcelable {
    private String name;
    private String surname;
    private String sex;
    private String email;
    private int age;
    private double weight ;
    private double height ;

    public User(){

    }

    public User(String name, String surname, String sex, String email, int age, double weight, double height) {
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.email = email;
        this.age = age;
        this.weight = weight;
        this.height = height;
    }

    protected User(Parcel in) {
        name = in.readString();
        surname = in.readString();
        sex = in.readString();
        email = in.readString();
        age = in.readInt();
        weight = in.readDouble();
        height = in.readDouble();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public void setData(String name, String surname, String sex, int age, String email, double weight, double height){
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.age = age;
        this.email = email;
        this.weight = weight;
        this.height = height;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(sex);
        dest.writeString(email);
        dest.writeInt(age);
        dest.writeDouble(weight);
        dest.writeDouble(height);
    }


    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }


}
