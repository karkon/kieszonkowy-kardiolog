package com.michalt.kieszonkowykardiolog.Model;

import java.util.Date;

public class MyMeasurementData  {
 private String measurementName; // name with time and date
 private String measurementType;
 private Date date;
 private int iD;

 public MyMeasurementData(String measurementName, String measurementType, Date date, int iD) {
  this.measurementName = measurementName;
  this.measurementType = measurementType;
  this.date = date;
  this.iD = iD;
 }

 public MyMeasurementData() {
 }

 public String getMeasurementName() {
  return measurementName;
 }

 public void setMeasurementName(String measurementName) {
  this.measurementName = measurementName;
 }

 public String getMeasurementType() {
  return measurementType;
 }

 public void setMeasurementType(String measurementType) {
  this.measurementType = measurementType;
 }

 public Date getDate() {
  return date;
 }

 public void setDate(Date date) {
  this.date = date;
 }

 public int getiD() {
  return iD;
 }

 public void setiD(int iD) {
  this.iD = iD;
 }



}
