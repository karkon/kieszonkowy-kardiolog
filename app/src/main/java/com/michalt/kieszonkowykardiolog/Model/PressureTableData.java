package com.michalt.kieszonkowykardiolog.Model;

import android.icu.text.SimpleDateFormat;

import java.util.Date;

public class PressureTableData {

    private String iD;
    private Date date;
    private String dateString;
    private String highBloodPressure;
    private String trestbps;
    private String thalach;

    public PressureTableData() {
    }

    public PressureTableData(String iD, Date date, String highBloodPressure, String trestbps, String thalach) {
        this.iD = iD;
        this.date = date;
        this.highBloodPressure = highBloodPressure;
        this.trestbps = trestbps;
        this.thalach = thalach;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
        dateString = dateFormat.format(date);
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHighBloodPressure() {
        return highBloodPressure;
    }

    public void setHighBloodPressure(String highBloodPressure) {
        this.highBloodPressure = highBloodPressure;
    }

    public String getTrestbps() {
        return trestbps;
    }

    public void setTrestbps(String trestbps) {
        this.trestbps = trestbps;
    }

    public String getThalach() {
        return thalach;
    }

    public void setThalach(String thalach) {
        this.thalach = thalach;
    }
}
