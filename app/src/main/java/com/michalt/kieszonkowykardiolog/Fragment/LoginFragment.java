package com.michalt.kieszonkowykardiolog.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.michalt.kieszonkowykardiolog.R;

public class LoginFragment extends Fragment {
    private Callback callback;
    private OnDataPass dataPasser;
    private Button loginButton;
    private Button registerButton;
    private EditText password;
    private EditText email;
    private String [] data;

    public LoginFragment() {
    }

    public interface Callback {
        public void onButtonClicked(View v);
    }

    public interface OnDataPass {
        public void onDataPass(String[] data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        loginButton = (Button) view.findViewById(R.id.loginButtonID);
        password = (EditText) view.findViewById(R.id.passwordEditTextID);
        email = (EditText) view.findViewById(R.id.emailEditTextID);
        registerButton = (Button) view.findViewById(R.id.createAccountButtonID);
        data = new String [2];
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    data [0]= password.getText().toString();
                    data [1]= email.getText().toString();
                    dataPasser.onDataPass(data);
                    callback.onButtonClicked(v);
                }
            }
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onButtonClicked(v);
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnDataPass) context;
        callback = (Callback) context;
    }

    @Override
    public void onDetach() {
        callback = null;
        super.onDetach();
    }
}
