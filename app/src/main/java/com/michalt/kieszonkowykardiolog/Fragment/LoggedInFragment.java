package com.michalt.kieszonkowykardiolog.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.michalt.kieszonkowykardiolog.R;

public class LoggedInFragment extends Fragment {
   private Callback callback;
    private Button logoutButton;
    private Button startButton;

    public LoggedInFragment() {

    }
    public interface Callback {
        public void onButtonClicked(View v);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logged_in, container, false);
        logoutButton = (Button) view.findViewById(R.id.LogOutButtonID);
        startButton = (Button) view.findViewById(R.id.StartButtonID);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onButtonClicked(v);
                }
            }
        });
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onButtonClicked(v);
                }
            }
        });

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (Callback) context;
         }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }
 }
