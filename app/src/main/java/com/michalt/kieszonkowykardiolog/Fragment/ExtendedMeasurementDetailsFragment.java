package com.michalt.kieszonkowykardiolog.Fragment;


import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.michalt.kieszonkowykardiolog.Model.ExtendedMeasurement;
import com.michalt.kieszonkowykardiolog.Model.SimpleMeasurement;
import com.michalt.kieszonkowykardiolog.R;

public class ExtendedMeasurementDetailsFragment extends Fragment {

    private ExtendedMeasurement measurement;

    private TextView date;
    private TextView age;
    private TextView weight;
    private TextView height;
    private TextView highPressure;
    private TextView lowPressure;
    private TextView heartRate;
    private TextView bloodSugar;
    private TextView cholesterol;
    private TextView chestPainType;
    private TextView painType;



    public ExtendedMeasurementDetailsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_extended_measurement_details, container, false);
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle != null){
            measurement = bundle.getParcelable("measurement");
        }
        setupUI(view );
        showData();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
    private void setupUI(View v) {
        date = v.findViewById(R.id.DateValueExtendedMeasurementDetailsID);
        age = v.findViewById(R.id.ageValueExtendedMeasurementDetailsID);
        weight = v.findViewById(R.id.weightValueExtendedMeasurementDetailsID);
        height = v.findViewById(R.id.heightValueExtendedMeasurementDetailsID);
        highPressure = v.findViewById(R.id.highPressureValueExtendedMeasurementDetailsID);
        lowPressure =v.findViewById(R.id.lowPressureValueExtendedMeasurementDetailsID);
        heartRate = v.findViewById(R.id.heartRateValueExtendedMeasurementDetailsID);
        bloodSugar = v.findViewById(R.id.bloodSugarValueExtendedMeasurementDetailsID);
        cholesterol = v.findViewById(R.id.cholesterolValueExtendedMeasurementDetailsID);
        chestPainType = v.findViewById(R.id.ChestPainTypeValueExtendedMeasurementDetailsID);
        painType = v.findViewById(R.id.PainTypeValueExtendedMeasurementDetailsID);



    }
    private void showData(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy_HH:mm");
        date.setText(dateFormat.format(measurement.getDateTime()));
        age.setText(String.valueOf(measurement.getAge()));
        weight.setText(String.valueOf(measurement.getWeight()));
        height.setText(String.valueOf(measurement.getHeight()));
        highPressure.setText(String.valueOf(measurement.getHighBloodPressure()));
        lowPressure.setText(String.valueOf(measurement.getThalach()));
        heartRate.setText(String.valueOf(measurement.getTrestbps()));
        bloodSugar.setText(String.valueOf(measurement.getFbs()));
        cholesterol.setText(String.valueOf(measurement.getChol()));
        chestPainType.setText(String.valueOf(measurement.getCp()));
        painType.setText(String.valueOf(measurement.getExang()));
    }



}
