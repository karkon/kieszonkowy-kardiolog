package com.michalt.kieszonkowykardiolog.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.michalt.kieszonkowykardiolog.Model.PressurePlotData;
import com.michalt.kieszonkowykardiolog.R;

import java.util.ArrayList;
import java.util.List;

// fragment wykorzystywany w statistic actiity oraz w oknie Clasify Result
public class PressurePlotFragment extends Fragment {
    String period;
    private LineChart pressurePlot;
    private List<Entry> lowPressureEntryList;
    private List<Entry> highPressureEntryList;
    private List<String> plotLabelsList;
    List<PressurePlotData> pressurePlotDataList;


    public PressurePlotFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pressure_plot, container, false);
        pressurePlot = view.findViewById(R.id.PressureChartID);
        pressurePlot.clear();
        Bundle bundle = getArguments();
        if (bundle != null) {
            pressurePlotDataList = bundle.getParcelableArrayList("data");
        }else
            pressurePlotDataList =  new ArrayList<>();
        if(pressurePlotDataList.size()>=2)
            prepareData(pressurePlotDataList);
        else
            pressurePlot.setNoDataText("Za mało pomiarów do wyświetlenia");
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void setupData() {

        LineDataSet highPressureDataSet = new LineDataSet(highPressureEntryList, this.getString(R.string.highPressureDataTittle));
        highPressureDataSet.setColor(Color.RED);
        highPressureDataSet.setDrawCircles(true);

        LineDataSet lowPressureDataSet = new LineDataSet(lowPressureEntryList,  this.getString(R.string.lowPressureDataTittle));
        lowPressureDataSet.setColor(Color.BLUE);
        lowPressureDataSet.setDrawCircles(true);

        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(highPressureDataSet);
        dataSets.add(lowPressureDataSet);
        LineData data = new LineData(dataSets);
        data.setDrawValues(false);
        pressurePlot.setData(data);
        pressurePlot.setNoDataText("Brak danych");
    }

    private void setupAxisXY() {
        final String[] labels = (String[]) plotLabelsList.toArray(new String[0]);
        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                return labels[(int) value];
            }
        };
        XAxis xAxis = pressurePlot.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);

        pressurePlot.getAxisRight().setEnabled(false);

        YAxis leftAxis = pressurePlot.getAxisLeft();
        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(200f);
        leftAxis.setLabelCount(10);


    }

    private void prepareData(List<PressurePlotData> pressurePlotDataList) {
        highPressureEntryList = new ArrayList<>();
        lowPressureEntryList = new ArrayList<>();
        plotLabelsList = new ArrayList<>();
       // plotLabelsList.add("");
        for (int i = 0; i < pressurePlotDataList.size(); i++) {
            PressurePlotData tmp = pressurePlotDataList.get(i);
            Entry tmphigh = new Entry(i, (float) tmp.getHighBloodPressure());
            Entry tmplow = new Entry(i, (float) tmp.getTrestbps());
            lowPressureEntryList.add(tmplow);
            highPressureEntryList.add(tmphigh);
            plotLabelsList.add(tmp.getDateString());
        }
       // plotLabelsList.add("");
        setupData();
        setupAxisXY();
        pressurePlot.invalidate();
    }




}
