package com.michalt.kieszonkowykardiolog.Fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.michalt.kieszonkowykardiolog.Constants;
import com.michalt.kieszonkowykardiolog.Model.ExtendedMeasurement;
import com.michalt.kieszonkowykardiolog.Model.SimpleMeasurement;
import com.michalt.kieszonkowykardiolog.R;

import java.util.Date;


public class ClassifyResultsFragment extends Fragment {

    Resources resources;
    private ImageView heartImage;
    private TextView cv1Title;
    private TextView cv1Val;
    private TextView cv2Title;
    private TextView cv2Val;
    private TextView cv3Title;
    private TextView cv3Val;
    private TextView cv4Title;
    private TextView cv4Val;
    private TextView cv5Title;
    private TextView cv5Val;
    private TextView cv6Title;
    private TextView cv6Val;
    private SimpleMeasurement measurement;
    private int prediction;
    Boolean isSimple = true;

    public ClassifyResultsFragment(boolean isSimple, SimpleMeasurement measurement, int prediction) {
        this.isSimple = isSimple;
        if(isSimple)
        this.measurement=measurement;
        else
        this.measurement=(ExtendedMeasurement)measurement;
        this.prediction=prediction;
    }

    public ClassifyResultsFragment(SimpleMeasurement measurement, int prediction) {
     this.measurement=measurement;
     this.prediction=prediction;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resources = getResources();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_classify_results, container, false);
        heartImage = (ImageView) view.findViewById(R.id.heartImageID);
        setupUI(view);
        setResultsValues(measurement, prediction);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void setupUI(View v) {
        cv1Title = (TextView) v.findViewById(R.id.textViewCV1Title);
        cv1Val = (TextView) v.findViewById(R.id.textViewCV1Val);
        cv2Title = (TextView) v.findViewById(R.id.textViewCV2Title);
        cv2Val = (TextView) v.findViewById(R.id.textViewCV2Val);
        cv3Title = (TextView) v.findViewById(R.id.textViewCV3Title);
        cv3Val = (TextView) v.findViewById(R.id.textViewCV3Val);
        cv4Title = (TextView) v.findViewById(R.id.textViewCV4Title);
        cv4Val = (TextView) v.findViewById(R.id.textViewCV4Val);
        cv5Title = (TextView) v.findViewById(R.id.textViewCV5Title);
        cv5Val = (TextView) v.findViewById(R.id.textViewCV5Val);
        cv6Title = (TextView) v.findViewById(R.id.textViewCV6Title);
        cv6Val = (TextView) v.findViewById(R.id.textViewCV6Val);
    }

    private void setHeartImage(int prediction) {
        switch (prediction) {
            case 0:
                heartImage.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.heart_0, null));
                break;
            case 1:
                heartImage.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.heart_1, null));
                break;
            case 2:
                heartImage.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.heart_2, null));
                break;
            case 3:
                heartImage.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.heart_3, null));
                break;
            case 4:
                heartImage.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.heart_4, null));
                break;
            default:
                heartImage.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.search, null));
                break;

        }
    }

    private void checkTrestbpsConditionFromConstants(double value) {
        if (value >= 0) {
            cv3Val.setText(String.valueOf(value));

            if (value >= Constants.TRESTBPS_NORMAL_MIN && value <= Constants.TRESTBPS_NORMAL_MAX) {
                //Ciśnienie prawidłowe
                cv3Val.setTextColor(getContext().getColor(R.color.green1));
            } else if (value >= Constants.TRESTBPS_HIGH_NORMAL_MIN && value <= Constants.TRESTBPS_HIGH_NORMAL_MAX) {
                //Ciśnienie wysokie prawidłowe
                cv3Val.setTextColor(getContext().getColor(R.color.green2));
            } else if (value >= Constants.TRESTBPS_ABOVE_1_MIN && value <= Constants.TRESTBPS_ABOVE_1_MAX) {
                //Nadciśnienie tętnicze 1 stopnia
                cv3Val.setTextColor(getContext().getColor(R.color.red1));
            } else if (value >= Constants.TRESTBPS_ABOVE_2_MIN && value <= Constants.TRESTBPS_ABOVE_2_MAX) {
                //Nadciśnienie tętnicze 2 stopnia
                cv3Val.setTextColor(getContext().getColor(R.color.red2));
            } else if (value >= Constants.TRESTBPS_ABOVE_3_MIN) {
                //Nadciśnienie tętnicze 3 stopnia
                cv3Val.setTextColor(getContext().getColor(R.color.red3));
            } else {
                //Ciśnienie spoczynkowe niskie
                cv3Val.setTextColor(getContext().getColor(R.color.blue1));
            }
        }
    }

    private void checkHighbpsConditionFromConstants(double value) { //TODO zastanowic sie nad nazwami tu i w constants
        if (value >= 0) {
            cv2Val.setText(String.valueOf(value));

            if (value >= Constants.HIGHBPS_NORMAL_MIN && value <= Constants.HIGHBPS_NORMAL_MAX) {
                //Ciśnienie prawidłowe
                cv2Val.setTextColor(getContext().getColor(R.color.green1));
            } else if (value >= Constants.HIGHBPS_HIGH_NORMAL_MIN && value <= Constants.HIGHBPS_HIGH_NORMAL_MAX) {
                //Ciśnienie wysokie prawidłowe
                cv2Val.setTextColor(getContext().getColor(R.color.green2));
            } else if (value >= Constants.HIGHBPS_ABOVE_1_MIN && value <= Constants.HIGHBPS_ABOVE_1_MAX) {
                //Nadciśnienie tętnicze 1 stopnia
                cv2Val.setTextColor(getContext().getColor(R.color.red1));
            } else if (value >= Constants.HIGHBPS_ABOVE_2_MIN && value <= Constants.HIGHBPS_ABOVE_2_MAX) {
                //Nadciśnienie tętnicze 2 stopnia
                cv2Val.setTextColor(getContext().getColor(R.color.red2));
            } else if (value >= Constants.HIGHBPS_ABOVE_3_MIN) {
                //Nadciśnienie tętnicze 3 stopnia
                cv2Val.setTextColor(getContext().getColor(R.color.red3));
            } else {
                //Cisnienie niskie
                cv2Val.setTextColor(getContext().getColor(R.color.blue1));
            }
        }
    }

    private void checkCholesterolConditionFromConstants(double value) {
        if (value >= 0) {
            cv5Val.setText(String.valueOf(value));

            if (value >= Constants.CHOLESTEROL_MIN && value <= Constants.CHOLESTEROL_MAX) {
                //Cholesterol prawidłowy
                cv5Val.setTextColor(getContext().getColor(R.color.green1));
            } else {
                //cholesterol nieprawidłowy
                cv5Val.setTextColor(getContext().getColor(R.color.red3));
            }
        }
    }

    private void checkBloodSugarConditionFromConstants(double value) {
        if (value >= 0) {
            cv6Val.setText(String.valueOf(value));

            if (value >= Constants.BLOOD_SUGAR_NORMAL_MIN && value <= Constants.BLOOD_SUGAR_NORMAL_MAX) {
                //Prawidłowy
                cv6Val.setTextColor(getContext().getColor(R.color.green1));
            } else if (value >= Constants.BLOOD_SUGAR_ABOVE_MIN && value <= Constants.BLOOD_SUGAR_ABOVE_MAX) {
                //Podwyższony
                cv6Val.setTextColor(getContext().getColor(R.color.orange1));
            } else if (value >= Constants.BLOOD_SUGAR_DIABETES_MIN) {
                //Podejrzenie cukrzycy (powtórny wynik potwierdzeniem cukrzycy)
                cv6Val.setTextColor(getContext().getColor(R.color.red3));
            } else {
                //Zbyt niski poziom cukru
                cv6Val.setTextColor(getContext().getColor(R.color.blue1));
            }
        }
    }

    private void checkHeartRateConditionFromConstants(double value) {
        if (value >= 0) {
            cv4Val.setText(String.valueOf(value));

            if (value >= Constants.HEART_RATE_MIN && value <= Constants.HEART_RATE_MAX) {
                //Puls prawidłowy
                cv4Val.setTextColor(getContext().getColor(R.color.green1));
            } else {
                //Puls nieprawidłowy
                cv4Val.setTextColor(getContext().getColor(R.color.red3));
            }
        }
    }

    private void checkBMIConditionFromConstants(double value) {
        if (value >= 0) {
            cv1Val.setText(String.valueOf(value));

            if (value < Constants.BMI_0) {
                //Niedowaga
                cv1Val.setTextColor(getContext().getColor(R.color.blue1));
            } else if (value >= Constants.BMI_0 && value < Constants.BMI_1) {
                //Waga prawidłowa
                cv1Val.setTextColor(getContext().getColor(R.color.green1));
            } else if (value >= Constants.BMI_1 && value < Constants.BMI_2) {
                //Nadwaga
                cv1Val.setTextColor(getContext().getColor(R.color.orange1));
            } else if (value >= Constants.BMI_2) {
                //Otyłość
                cv1Val.setTextColor(getContext().getColor(R.color.red3));
            }
        }
    }

    private double calculateBMI(double weight, double height) {
        return Math.round(weight / Math.pow(height / 100, 2));

    }

    public void checkConditionsFromConstants(double trestbps, double highbps, double cholesterol, double bloodSugar, double heartRate, double bmi) {
        checkTrestbpsConditionFromConstants(trestbps);
        checkHighbpsConditionFromConstants(highbps);
        checkCholesterolConditionFromConstants(cholesterol);
        checkBloodSugarConditionFromConstants(bloodSugar);
        checkHeartRateConditionFromConstants(heartRate);
        checkBMIConditionFromConstants(bmi);
    }

    private  void setResultsValues(SimpleMeasurement measurement, int prediction) {
        setHeartImage(prediction);
        double bmi = calculateBMI(measurement.getWeight(), measurement.getHeight());

        //if bierzemy z konfiguracji
        if(isSimple)
        checkConditionsFromConstants(measurement.getTrestbps(), measurement.getHighBloodPressure(), -1, -1, measurement.getThalach(), bmi);
        else
            checkConditionsFromConstants(measurement.getTrestbps(), measurement.getHighBloodPressure(), ((ExtendedMeasurement )measurement).getChol(), ((ExtendedMeasurement)measurement).getFbs(), measurement.getThalach(), bmi);
    }

}
