package com.michalt.kieszonkowykardiolog.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.michalt.kieszonkowykardiolog.Model.PressureTableData;
import com.michalt.kieszonkowykardiolog.R;

import java.util.List;

public class MeasurementsTableAdapter extends RecyclerView.Adapter<MeasurementsTableAdapter.ViewHolder>   {
    private Context context;
    private List<PressureTableData> pressureTableDataList;

    public MeasurementsTableAdapter(Context context, List<PressureTableData> pressureTableDataList ) {
        this.context = context;
        this.pressureTableDataList = pressureTableDataList;

    }

    @NonNull
    @Override
    public MeasurementsTableAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_measurmeant_table_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MeasurementsTableAdapter.ViewHolder holder, int position) {
        PressureTableData item = pressureTableDataList.get(position);
        holder.iDTextView.setText(item.getiD());
        holder.dateTextView.setText(item.getDateString());
        holder.trestbpsTextView.setText(item.getTrestbps());
        holder.thalachTextView.setText(item.getThalach());
        holder.highBloodPressureTextView.setText(item.getHighBloodPressure());
        if(position==0){




        }






    }

    @Override
    public int getItemCount() {
        return pressureTableDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView iDTextView;
        TextView dateTextView;
        TextView highBloodPressureTextView;
        TextView trestbpsTextView;
        TextView thalachTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iDTextView = itemView.findViewById(R.id.RowID);
            dateTextView =  itemView.findViewById(R.id.RowDateID);
            highBloodPressureTextView =  itemView.findViewById(R.id.RowHighPressureID);
            thalachTextView =  itemView.findViewById(R.id.RowLowPressureID);
            trestbpsTextView  = itemView.findViewById(R.id.RowHeartRateID);
        }
    }

}
