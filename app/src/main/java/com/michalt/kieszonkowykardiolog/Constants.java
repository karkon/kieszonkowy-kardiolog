package com.michalt.kieszonkowykardiolog;

public class Constants {
    public final static int TRESTBPS_NORMAL_MIN = 80;
    public final static int TRESTBPS_NORMAL_MAX = 84;
    public final static int TRESTBPS_HIGH_NORMAL_MIN = 85;
    public final static int TRESTBPS_HIGH_NORMAL_MAX = 89;
    public final static int TRESTBPS_ABOVE_1_MIN = 90;
    public final static int TRESTBPS_ABOVE_1_MAX = 99;
    public final static int TRESTBPS_ABOVE_2_MIN = 100;
    public final static int TRESTBPS_ABOVE_2_MAX = 109;
    public final static int TRESTBPS_ABOVE_3_MIN = 110;

    public final static int HIGHBPS_NORMAL_MIN = 120;
    public final static int HIGHBPS_NORMAL_MAX = 129;
    public final static int HIGHBPS_HIGH_NORMAL_MIN = 130;
    public final static int HIGHBPS_HIGH_NORMAL_MAX = 139;
    public final static int HIGHBPS_ABOVE_1_MIN = 140;
    public final static int HIGHBPS_ABOVE_1_MAX = 159;
    public final static int HIGHBPS_ABOVE_2_MIN = 160;
    public final static int HIGHBPS_ABOVE_2_MAX = 179;
    public final static int HIGHBPS_ABOVE_3_MIN = 180;

    public final static int CHOLESTEROL_MIN = 125;
    public final static int CHOLESTEROL_MAX = 200;

    public final static double BLOOD_SUGAR_NORMAL_MIN = 70;
    public final static double BLOOD_SUGAR_NORMAL_MAX = 99;
    public final static double BLOOD_SUGAR_ABOVE_MIN = 100;
    public final static double BLOOD_SUGAR_ABOVE_MAX = 125;
    public final static double BLOOD_SUGAR_DIABETES_MIN = 126;

    public final static int HEART_RATE_MIN = 60;
    public final static int HEART_RATE_MAX = 100;

    public final static double BMI_0 = 18.5;
    public final static double BMI_1 = 25;
    public final static double BMI_2 = 30;

    //todo dodac norme cukru
}
