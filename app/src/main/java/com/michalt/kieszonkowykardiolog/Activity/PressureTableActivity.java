package com.michalt.kieszonkowykardiolog.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.michalt.kieszonkowykardiolog.Adapter.MeasurementsTableAdapter;
import com.michalt.kieszonkowykardiolog.Model.PressureTableData;
import com.michalt.kieszonkowykardiolog.Model.User;
import com.michalt.kieszonkowykardiolog.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Pressure table activity.
 */
public class PressureTableActivity extends AppCompatActivity{
    private int iD;
    private User user;
    private List <PressureTableData> myMeasurementDataList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter myAdapter;
    private FirebaseFirestore databaseInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pressure_table);
        Bundle bundle = getIntent().getExtras();
        myMeasurementDataList = new ArrayList<>();
        recyclerView = findViewById(R.id.PressureTableRecyclerViewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(bundle != null){
            user = bundle.getParcelable("user");
        }
        myAdapter = new MeasurementsTableAdapter(this, myMeasurementDataList);
        recyclerView.setAdapter(myAdapter);
    }
    @Override
    protected void onStart() {
        super.onStart();
        databaseInstance = FirebaseFirestore.getInstance();
        loadMeasurementsInfo();
    }


    private void loadMeasurementsInfo() {
        myMeasurementDataList.clear();
        databaseInstance.collection("users_measurements").document(user.getEmail()).collection("measurements")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int i = 1;
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Date date = document.getDate("date");
                                String thalach = String.valueOf(document.getDouble("thalach"));
                                String highBloodPressure = String.valueOf(document.getDouble("highBloodPressure"));
                                String trestbps = String.valueOf(document.getDouble("trestbps"));
                                PressureTableData tmp = new PressureTableData(String.valueOf(i),date,highBloodPressure,trestbps,thalach);
                                myMeasurementDataList.add(tmp);
                                i++;
                            }
                            sortMyMeasurementDataList();

                        } else {
                            Toast.makeText(PressureTableActivity.this, R.string.downloadError, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    private void sortMyMeasurementDataList(){
        myMeasurementDataList.sort((o1,o2) -> o2.getDate().compareTo(o1.getDate()));
        for(int j =0;j <myMeasurementDataList.size();j++){
            myMeasurementDataList.get(j).setiD(String.valueOf(j+1));
        }
        myAdapter.notifyDataSetChanged();
    }

}
