package com.michalt.kieszonkowykardiolog.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.michalt.kieszonkowykardiolog.Model.User;
import com.michalt.kieszonkowykardiolog.R;

public class MenuActivity extends AppCompatActivity  implements View.OnClickListener{
    private static final int REQUEST_CODE_ADD_ACTIVITY = 2 ;
    private Button statisticActivityButton;
    private Button addMeasurementActivityButton;
    private Button myMeasurementsButton;
    private Button pressureTableButton;
    private TextView usernameTextView;
    private ImageView userImage;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            user = bundle.getParcelable("user");
        }
        setupUI();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.StatisticButtonID:
            {
                startStatisticActivity();
                break;
            }
            case R.id.AddMeasurmeantButtonID:
            {
                startAddMeasurementActivity();
                break;
            }
            case R.id.MyMeasurmeantsButtonID:
            {
                startMyMeasurementsActivity();
                break;
            }
            case R.id.PressureTableButtonID:
            {
                startPressureTableActivity();
                break;
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){



    }
    private void setupUI(){
        statisticActivityButton = findViewById(R.id.StatisticButtonID);
        addMeasurementActivityButton = findViewById(R.id.AddMeasurmeantButtonID);
        myMeasurementsButton = findViewById(R.id.MyMeasurmeantsButtonID);
        pressureTableButton = findViewById(R.id.PressureTableButtonID);
        statisticActivityButton.setOnClickListener(this);
        addMeasurementActivityButton.setOnClickListener(this);
        myMeasurementsButton.setOnClickListener(this);
        pressureTableButton.setOnClickListener(this);
        usernameTextView = findViewById(R.id.NameTextID);
        userImage = findViewById(R.id.UserImageID);
        setUserImage(user.getSex());
        usernameTextView.setText(user.getName() +" "+ user.getSurname() );
    }

    private void startAddMeasurementActivity(){
        Intent addMeasurementIntent= (new Intent(MenuActivity.this, AddMeasurementActivity.class));
        addMeasurementIntent.putExtra("user",user);
        startActivity(addMeasurementIntent);    }
    private void startStatisticActivity(){
        Intent statisticActivityIntent= (new Intent(MenuActivity.this, StatisticActivity.class));
        statisticActivityIntent.putExtra("user",user);
        startActivity(statisticActivityIntent);
    }
    private void startMyMeasurementsActivity(){
        Intent myMeasurementActivityIntent= (new Intent(MenuActivity.this, MyMeasurementsActivity.class));
        myMeasurementActivityIntent.putExtra("user",user);
        startActivity(myMeasurementActivityIntent);
    }
    private void startPressureTableActivity(){
        Intent pressureTableActivityIntent= (new Intent(MenuActivity.this, ClassifyResultsActivity.class));
        pressureTableActivityIntent.putExtra("user",user);
        startActivity(pressureTableActivityIntent);
//        Intent myMeasurementActivityIntent= (new Intent(MenuActivity.this, PressureTableActivity.class));
//        myMeasurementActivityIntent.putExtra("user",user);
//        startActivity(myMeasurementActivityIntent);
    }

    private void setUserImage(String sex){
        if (sex.equals("Mężczyzna")){
            userImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.boy, null));
        }else if (sex.equals("Kobieta")){
            userImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.girl, null));
        }else{
            userImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.meerkat, null));
        }
    }


}



