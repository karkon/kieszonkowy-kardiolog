package com.michalt.kieszonkowykardiolog.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.michalt.kieszonkowykardiolog.Fragment.LoggedInFragment;
import com.michalt.kieszonkowykardiolog.Fragment.LoginFragment;
import com.michalt.kieszonkowykardiolog.R;
import com.michalt.kieszonkowykardiolog.Model.User;

public class MainActivity extends AppCompatActivity implements LoginFragment.Callback, LoginFragment.OnDataPass, LoggedInFragment.Callback{

    private static final int REQUEST_CODE = 1;
    private Button creditsButton;
    private FirebaseAuth databaseAuth;
    private AlertDialog creditDialog;
    private String email;
    private String password;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUI();
          }
    @Override
    protected void onStart() {
        super.onStart();
        databaseAuth = FirebaseAuth.getInstance();
        changeFragmentToLoggedOut();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        Bundle bundle = data.getExtras();
        if(bundle != null){
            if (bundle.getBoolean("accountCreated"))
            user = bundle.getParcelable("user");
        }
    }
    @Override
    public void onDataPass(String[] data) {
        email = data[1];
        password = data[0];
    }
    @Override
    public void onButtonClicked(View v) {
        switch (v.getId()) {
            case R.id.loginButtonID:
            {
                loginUser(email,password);
                break;
            }
            case R.id.createAccountButtonID:
            {
                registerNewUser();
                break;
            }
            case R.id.LogOutButtonID:
            {
                logoutUser();
                changeFragmentToLoggedOut();
                break;
            }
            case R.id.StartButtonID:
            {
                Intent menuActivityIntent = new Intent(MainActivity.this,MenuActivity.class);
                menuActivityIntent.putExtra("user",user);
                startActivity(menuActivityIntent);
                break;
            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        logoutUser();
    }
    private void setupUI(){
        creditsButton = findViewById(R.id.creditsButtonID);
        creditsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCreditsDialog();
            }
        });

    }
    void readUseData(){
        try {
            FirebaseFirestore databaseInstance = FirebaseFirestore.getInstance();
            DocumentReference docRef = databaseInstance.collection("users").document(email);
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            try {
                               String name = document.getString("name");
                               String surname = document.getString("surname");
                               String email = document.getString("email");
                               String sex = document.getString("sex");
                               int age = document.getDouble("age").intValue();
                               double weight = document.getDouble("weight");
                               double height = document.getDouble("height");
                               user = new User( name,  surname,  sex, email, age,weight,height);
                               changeFragmentToLoggedIn();
                            } catch (Exception e1) {


                            }

                        } else {
                        }
                    } else {

                    }
                }
            });

    }catch (Exception e1){
        }
    }
   private void loginUser( String email,String password){
        if (!email.isEmpty() && !password.isEmpty()) {
            signInUser(email, password);
        } else
            Toast.makeText(MainActivity.this, R.string.emptyFields, Toast.LENGTH_SHORT).show();
    }
    private void signInUser(String email, String password){
        databaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, R.string.datalistLoading, Toast.LENGTH_SHORT).show();
                            readUseData();
                        } else {
                            Toast.makeText(MainActivity.this, R.string.loginError, Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

    private void registerNewUser(){
        Intent userRegisterActivityIntent = (new Intent(MainActivity.this, UserRegisterActivity.class));
        startActivityForResult(userRegisterActivityIntent, REQUEST_CODE);
    }
    private void logoutUser(){
        databaseAuth.signOut();
    }
    private void showCreditsDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_credits, null);
        Button okButton = view.findViewById(R.id.creditsButtonOkID);
        dialogBuilder.setView(view);
        creditDialog = dialogBuilder.create();
        creditDialog.show();
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                creditDialog.dismiss();
            }
        });
    }
    private void changeFragmentToLoggedIn(){
        Fragment newFragment = new LoggedInFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.loginFragmentHolderID, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
    private void changeFragmentToLoggedOut(){
        Fragment newFragment = new LoginFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.loginFragmentHolderID, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
    private void checkIsUserLoggedIn(){
        FirebaseUser currentUser = databaseAuth.getCurrentUser();
        if (currentUser == null) {
            changeFragmentToLoggedOut();
        } else {
            readUseData();
        }
    }

    @Override
    public void onBackPressed() {
            this.finish();
    }
}
