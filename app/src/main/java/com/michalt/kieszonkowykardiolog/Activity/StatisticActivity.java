package com.michalt.kieszonkowykardiolog.Activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.michalt.kieszonkowykardiolog.Fragment.HeartRatePlotFragment;
import com.michalt.kieszonkowykardiolog.Fragment.PressurePlotFragment;
import com.michalt.kieszonkowykardiolog.Model.HearthRatePlotData;
import com.michalt.kieszonkowykardiolog.Model.PressurePlotData;
import com.michalt.kieszonkowykardiolog.Model.User;
import com.michalt.kieszonkowykardiolog.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StatisticActivity extends FragmentActivity {
    private static final int NUM_PAGES = 2;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private FirebaseFirestore databaseInstance;
    private BottomNavigationView bottomNavigationView;
    private MenuItem prevMenuItem;
    private List<PressurePlotData> pressurePlotDataList;
    private List<HearthRatePlotData> hearthRatePlotDataList;
    private Spinner timeSpinner;
    private String period;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            user = bundle.getParcelable("user");
        }
        period = "1 tydzień";
        pressurePlotDataList = new ArrayList<>();
        hearthRatePlotDataList = new ArrayList<>();
        setupUI();
    }

    @Override
    public void onStart() {
        super.onStart();
        databaseInstance = FirebaseFirestore.getInstance();
        loadData();
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            this.finish();
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    private void setupUI() {
        bottomNavigationView = findViewById(R.id.StatisticNavigationVIewID);
        viewPager = (ViewPager) findViewById(R.id.StatisticViewPagerID);
        pagerAdapter = new StatisticActivity.ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        timeSpinner = findViewById(R.id.TimeChoiceSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.timeArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeSpinner.setAdapter(adapter);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.PressureMenuStatisticID:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.HearRateMenuButtonStatisticID:
                        viewPager.setCurrentItem(1);
                        return true;
                }
                return false;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null)
                    prevMenuItem.setChecked(false);
                else
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);

                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                period = timeSpinner.getItemAtPosition(position).toString();
                loadData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void loadData(){
        pressurePlotDataList.clear();
        hearthRatePlotDataList.clear();
        Calendar calendar = Calendar.getInstance();
        switch (period){
            case "1 tydzień":{
                calendar.add(Calendar.DATE, -7);
                break;
            }
            case "2 tygodnie":{
                calendar.add(Calendar.DATE, -14);
                break;
            }
            case "3 tygodnie":{
                calendar.add(Calendar.DATE, -21);
                break;
            }
            case "1 miesiąc":{
                calendar.add(Calendar.MONTH, -1);
                break;
            }
        }
        Date referenceDate = calendar.getTime();
        loadMeasurementsFromDatabase(referenceDate);
    }
    private void loadMeasurementsFromDatabase(   Date referenceDate) {
        List<PressurePlotData> pressureList = new ArrayList<>();
        List<HearthRatePlotData> hearthRateList = new ArrayList<>();
        databaseInstance.collection("users_measurements").document(user.getEmail()).collection("measurements")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                try {
                                    Date date = document.getDate("date");
                                    if (referenceDate.compareTo(date) <= 0) {
                                        double highBloodPreasure = document.getDouble("highBloodPressure"); //highBloodPreasure
                                        double trestbps = document.getDouble("trestbps"); // low blood Pressure
                                        double heartRate = document.getDouble("thalach");
                                        String name = document.getString("name");
                                        PressurePlotData tmp = new PressurePlotData(highBloodPreasure, trestbps, date);
                                        HearthRatePlotData tmp1 = new HearthRatePlotData(heartRate, date);
                                        pressureList.add(tmp);
                                        hearthRateList.add(tmp1);
                                    }
                                } catch(Exception | Error e){
                                    Log.d("myExcepction", e.toString());
                                }
                            }
                            pressureList.sort((o1, o2) -> o1.getDate().compareTo(o2.getDate()));
                            hearthRateList.sort((o1, o2) -> o1.getDate().compareTo(o2.getDate()));
                            pressurePlotDataList = pressureList;
                            hearthRatePlotDataList = hearthRateList;
                            pagerAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(StatisticActivity.this, R.string.downloadError, Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }




    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle;
            if (position == 1) {
                bundle = new Bundle();
                bundle.putParcelableArrayList("data", (ArrayList<? extends Parcelable>) hearthRatePlotDataList);
                Fragment heartRatePlotFragment = new HeartRatePlotFragment();
                heartRatePlotFragment.setArguments(bundle);
                return heartRatePlotFragment;
                //return new HeartRatePlotFragment(hearthRatePlotDataList);
            }
            bundle = new Bundle();
            bundle.putParcelableArrayList("data", (ArrayList<? extends Parcelable>) pressurePlotDataList);
            Fragment pressurePlotFragment = new PressurePlotFragment();
            pressurePlotFragment.setArguments(bundle);
            return pressurePlotFragment;
           // return new PressurePlotFragment(pressurePlotDataList);

        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

    }


}
