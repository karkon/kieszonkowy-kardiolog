package com.michalt.kieszonkowykardiolog.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.michalt.kieszonkowykardiolog.Fragment.AddExtendedMeasurementFragment;
import com.michalt.kieszonkowykardiolog.Fragment.AddSimpleMeasurementFragment;
import com.michalt.kieszonkowykardiolog.Model.User;
import com.michalt.kieszonkowykardiolog.R;
import com.michalt.kieszonkowykardiolog.Model.SimpleMeasurement;

import java.util.HashMap;
import java.util.Map;

public class AddMeasurementActivity extends FragmentActivity  implements AddExtendedMeasurementFragment.Callback, AddSimpleMeasurementFragment.Callback{
    private static final int REQUEST_CODE_CLASSIFY = 6;
    private static final int NUM_PAGES = 2;
    private SimpleMeasurement measurement;
    private AlertDialog.Builder DialogBuilder;
    private AlertDialog exitDialog;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private BottomNavigationView bottomNavigationView;
    private MenuItem prevMenuItem;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_measurmeant);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            user = bundle.getParcelable("user");
        }
        setupUI();
    }
    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            showExitConfirmationDialog();
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){



    }
    private void setupUI(){
        viewPager = (ViewPager) findViewById(R.id.AddMeasurementViewPagerID);
        pagerAdapter =  new AddMeasurementActivity.ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        bottomNavigationView = findViewById(R.id.AddMeasurmeantNavigationVIewID);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.SimpleMeasurmeantButtonID:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.ExtendedMeasurmeantButtonID:
                        viewPager.setCurrentItem(1);
                        return true;
                }
                return false;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null)
                    prevMenuItem.setChecked(false);
                else
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);

                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void backToMenuActivity() {
        Intent returnIntent = getIntent();
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void saveMeasurementData() {
        FirebaseFirestore databaseInstance = FirebaseFirestore.getInstance();
        Map<String, Object> measurementData = new HashMap<>();
        measurementData.put("type","simple");
        measurementData.put("name",measurement.getMeasurementName());
        measurementData.put("age",measurement.getAge());
        measurementData.put("sex",measurement.getSex());
        measurementData.put("thalach",measurement.getThalach());
        measurementData.put("highBloodPressure",measurement.getHighBloodPressure());
        measurementData.put("trestbps",measurement.getTrestbps());
        measurementData.put("date",measurement.getDateTime());
        measurementData.put("weight",measurement.getWeight());
        measurementData.put("height",measurement.getHeight());

        databaseInstance.collection("users_measurements").document(user.getEmail()).collection("measurements").document(measurement.getMeasurementName()).set(measurementData).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(AddMeasurementActivity.this, "Pomiar zapisany", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AddMeasurementActivity.this, "Błąd zapisywanie pomiaru", Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void showExitConfirmationDialog(){
        DialogBuilder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_exit_confirmaction, null);
        Button yesButton = view.findViewById(R.id.yesButtonID);
        Button noButton = view.findViewById(R.id.noButtonID);
        DialogBuilder.setView(view);
        exitDialog = DialogBuilder.create();
        exitDialog.show();
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitDialog.dismiss();
            }
        });

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToMenuActivity();
            }
        });
    }

    @Override
    public void onMeasureAdd(View v, SimpleMeasurement measurement) {
        this.measurement = measurement;
        switch (v.getId()) {
            case R.id.saveButtonSMID: {
                saveMeasurementData();
                startClassifyActivity(true);
                break;
            }
            case R.id.checkSaveButtonEMID: {
                startClassifyActivity(false);
                break;
            }
        }
    }
    private void startClassifyActivity(boolean isSimple){
        Intent classifyActivityIntent = (new Intent(AddMeasurementActivity.this, ClassifyResultsActivity.class));
        classifyActivityIntent.putExtra("user",user);
        classifyActivityIntent.putExtra("isSimple",isSimple);
        classifyActivityIntent.putExtra("measurement",measurement);
        startActivityForResult(classifyActivityIntent, REQUEST_CODE_CLASSIFY);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            Bundle bundle = new Bundle();
            bundle.putParcelable("user",user);
            if (position == 1) {
                fragment = new AddExtendedMeasurementFragment();
            }else {
                fragment  = new AddSimpleMeasurementFragment();
            }
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

}