package com.michalt.kieszonkowykardiolog.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.michalt.kieszonkowykardiolog.Fragment.ExtendedMeasurementDetailsFragment;
import com.michalt.kieszonkowykardiolog.Fragment.SimpleMeasurementDetailsFragment;
import com.michalt.kieszonkowykardiolog.Model.ExtendedMeasurement;
import com.michalt.kieszonkowykardiolog.Model.SimpleMeasurement;
import com.michalt.kieszonkowykardiolog.R;

import java.util.Date;

public class MeasurementDetailsActivity extends AppCompatActivity implements View.OnClickListener{
    private FirebaseFirestore databaseInstance;
    private Button backButton;
    private String username;
    private boolean isSimple;
    private String measurementName;
    private Fragment measurementDetailsFragment;
    private SimpleMeasurement measurement;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurmeant_details);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
             username = extras.getString("username");
             measurementName = extras.getString("measurement_name");
             isSimple = extras.getString("is_simple").equals("simple");

        }else
            Toast.makeText(MeasurementDetailsActivity.this, R.string.downloadError, Toast.LENGTH_SHORT).show();
        backButton = findViewById(R.id.BackFromMeasurmeantDetailsButtonID);
        backButton.setOnClickListener(this);
    }
    @Override
    protected void onStart(){
        super.onStart();
        databaseInstance = FirebaseFirestore.getInstance();
        loadMeasurementData();
       // measurement = new ExtendedMeasurement("1", 1, "1",68, 135, 67,new Date(), 90, 180,12,13,14,15,1);
    }
    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.BackFromMeasurmeantDetailsButtonID) {
            this.finish();
        }
    }
    @Override
    public void onBackPressed() {
            this.finish();
    }

    private void loadMeasurementData() {
        DocumentReference docRef = databaseInstance.collection("users_measurements").document(username).collection("measurements").document(measurementName);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            String measurementName = document.getString("name");
                            int age = document.getDouble("age").intValue() ;
                            String sex = document.getString("sex");
                            double thalach = document.getDouble("thalach");
                            double highBloodPressure =document.getDouble("highBloodPressure");
                            double trestbps = document.getDouble("trestbps");
                            Date dateTime = document.getDate("date");
                            double weight = document.getDouble("weight");
                            double height = document.getDouble("height");
                            if (!isSimple){
                                double cp = document.getDouble("cp");
                                double chol = document.getDouble("chol");
                                double fbs = document.getDouble("fbs");
                                double exang = document.getDouble("exang");
                                int prediction = document.getDouble("prediction").intValue();
                                measurement = new ExtendedMeasurement(measurementName,age,sex,thalach,highBloodPressure,trestbps,dateTime,weight,height,cp,chol,fbs,exang,prediction);
                            }else{
                                measurement = new SimpleMeasurement(measurementName,age,sex,thalach,highBloodPressure,trestbps,dateTime,weight,height);
                            }
                            showData();


                        } catch (Exception e1) {
                            Toast.makeText(MeasurementDetailsActivity.this,R.string.dataDownloadError, Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Toast.makeText(MeasurementDetailsActivity.this,R.string.dataDownloadError, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MeasurementDetailsActivity.this,R.string.dataDownloadError, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
    private void showData(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("measurement",measurement);
        if(isSimple) {
            measurementDetailsFragment = new SimpleMeasurementDetailsFragment();
        }
       else
            measurementDetailsFragment = new ExtendedMeasurementDetailsFragment();
        measurementDetailsFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.MeasurmeantDetailsFragmenmtHolder, measurementDetailsFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }



}
