package com.michalt.kieszonkowykardiolog.Activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.michalt.kieszonkowykardiolog.Fragment.ClassifyResultsFragment;
import com.michalt.kieszonkowykardiolog.Fragment.HeartRatePlotFragment;
import com.michalt.kieszonkowykardiolog.Fragment.PressurePlotFragment;
import com.michalt.kieszonkowykardiolog.Model.ExtendedMeasurement;
import com.michalt.kieszonkowykardiolog.Model.HearthRatePlotData;
import com.michalt.kieszonkowykardiolog.Model.PressurePlotData;
import com.michalt.kieszonkowykardiolog.Model.SimpleMeasurement;
import com.michalt.kieszonkowykardiolog.Model.User;
import com.michalt.kieszonkowykardiolog.R;
import com.michalt.kieszonkowykardiolog.Utility.WekaClassifier;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassifyResultsActivity extends FragmentActivity {

    private static final int NUM_PAGES = 3;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private FirebaseFirestore databaseInstance;
    private List<PressurePlotData> pressurePlotDataList;
    private List<HearthRatePlotData> hearthRatePlotDataList;
    private BottomNavigationView bottomNavigationView;
    private MenuItem prevMenuItem;
    private double prediction;
    private SimpleMeasurement measurement;
    private String period;
    private User user;
    private boolean isSimple;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classify_results);
        pressurePlotDataList = new ArrayList<>();
        hearthRatePlotDataList = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        isSimple= false;
        if(bundle != null){
            user = bundle.getParcelable("user");
            isSimple = bundle.getBoolean("isSimple");
            if(!isSimple)
            measurement = (ExtendedMeasurement)bundle.getParcelable("measurement");
            else
            measurement = bundle.getParcelable("measurement");
            isSimple = bundle.getBoolean("isSimple");

        }
        // todo tymczasowy pomiar
      measurement = new ExtendedMeasurement(43,"Merzczyzna",10,120,150,75,177,4,489,180,1);
        prediction =5;
        period = "1 tydzień";
        if(!isSimple) {
            classify();
        }
        setupUI();

    }

    @Override
    public void onStart() {
        super.onStart();
        databaseInstance = FirebaseFirestore.getInstance();
        loadData();
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            this.finish();
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    private void setupUI() {
        bottomNavigationView = findViewById(R.id.ClasifyResultsNavigationVIewID);
        viewPager = (ViewPager) findViewById(R.id.ClasifyResultsViewPagerID);
        pagerAdapter = new ClassifyResultsActivity.ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.ResultsMenuButtonID:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.PressureMenuButtonID:
                        viewPager.setCurrentItem(1);
                        return true;
                    case R.id.HearRateMenuButtonID:
                        viewPager.setCurrentItem(2);
                        return true;
                }
                return false;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null)
                    prevMenuItem.setChecked(false);
                else
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);

                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void classify() {
        WekaClassifier classifier = new WekaClassifier(9, 1, getAssets());
        try {
            int sex;
            int sugar;
            if(user.getSex().equals("Kobieta"))
                sex= 0;
            else
                sex = 1;
            if (((ExtendedMeasurement) measurement).getFbs()>=120)
                sugar = 1;
            else sugar = 0;
           // prediction = classifier.classify(8, measurement.getAge(),sex ,((ExtendedMeasurement) measurement).getCp() , measurement.getHighBloodPressure(), ((ExtendedMeasurement) measurement).getChol(),sugar , measurement.getThalach(), ((ExtendedMeasurement) measurement).getExang());
            prediction = classifier.classify(8,64,1,4,145,212,0,132,0); // todo tutaj wpisuj dane
         //   saveMeasurementData();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("weka", e.toString());
        }
    }
    private void saveMeasurementData() {
        FirebaseFirestore databaseInstance = FirebaseFirestore.getInstance();
        Map<String, Object> measurementData = new HashMap<>();
        measurementData.put("type","extended");
        measurementData.put("cp", ((ExtendedMeasurement) measurement).getCp());
        measurementData.put("chol",((ExtendedMeasurement) measurement).getChol());
        measurementData.put("fbs",((ExtendedMeasurement) measurement).getFbs());
        measurementData.put("exang",((ExtendedMeasurement) measurement).getExang());
        measurementData.put("prediction",prediction);
        measurementData.put("name",measurement.getMeasurementName());
        measurementData.put("age",measurement.getAge());
        measurementData.put("sex",measurement.getSex());
        measurementData.put("thalach",measurement.getThalach());
        measurementData.put("highBloodPressure",measurement.getHighBloodPressure());
        measurementData.put("trestbps",measurement.getTrestbps());
        measurementData.put("date",measurement.getDateTime());
        measurementData.put("weight",measurement.getWeight());
        measurementData.put("height",measurement.getHeight());

        databaseInstance.collection("users_measurements").document(user.getEmail()).collection("measurements").document(measurement.getMeasurementName()).set(measurementData).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                Toast.makeText(AddMeasurementActivity.this, "ok", Toast.LENGTH_SHORT).show();
//                backToMenuActivity();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(AddMeasurementActivity.this, "nok", Toast.LENGTH_SHORT).show();
            }
        });

    }

private void loadData(){

    pressurePlotDataList.clear();
    hearthRatePlotDataList.clear();
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -14);
    Date referenceDate = calendar.getTime();
    loadMeasurementsFromDatabase(referenceDate);
}
    private void loadMeasurementsFromDatabase(   Date referenceDate) {

        databaseInstance.collection("users_measurements").document(user.getEmail()).collection("measurements")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                try {
                                    Date date = document.getDate("date");
                                    if (referenceDate.compareTo(date) <= 0) {
                                        double highBloodPressure = document.getDouble("highBloodPressure"); //highBloodPressure
                                        double trestbps = document.getDouble("trestbps"); // low blood Pressure
                                        double heartRate = document.getDouble("thalach");
                                        String name = document.getString("name");
                                        PressurePlotData tmp = new PressurePlotData(highBloodPressure, trestbps, date);
                                        HearthRatePlotData tmp1 = new HearthRatePlotData(heartRate, date);
                                        pressurePlotDataList.add(tmp);
                                        hearthRatePlotDataList.add(tmp1);
                                    }
                                }
                                catch(Exception | Error e){
                                    Log.d("myExcepction", e.toString());
                                }
                            }
                            pressurePlotDataList.sort((o1, o2) -> o1.getDate().compareTo(o2.getDate()));
                            hearthRatePlotDataList.sort((o1, o2) -> o1.getDate().compareTo(o2.getDate()));
                            pagerAdapter.notifyDataSetChanged();

                        } else {
                            Toast.makeText(ClassifyResultsActivity.this, R.string.downloadError, Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle;
            switch (position) {
                case 1:
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("data", (ArrayList<? extends Parcelable>) pressurePlotDataList);
                    Fragment pressurePlotFragment = new PressurePlotFragment();
                    pressurePlotFragment.setArguments(bundle);
                    return pressurePlotFragment;
                case 2:
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("data", (ArrayList<? extends Parcelable>) hearthRatePlotDataList);
                    Fragment heartRatePlotFragment = new HeartRatePlotFragment();
                    heartRatePlotFragment.setArguments(bundle);
                    return heartRatePlotFragment;
                default:
                    return new ClassifyResultsFragment(isSimple,measurement, (int) prediction);
            }

        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

    }


}
